extends Node

var inventory : Inventory = Inventory.new(3)
var item1 : Item = Item.new("Item1", 0)
var item2 : Item = Item.new("Item2", 3)
var item3 : Item = Item.new("Item3", 2)


func _ready():
	print(inventory)
	print(item1)
	print(item2)
	print(item3)
	var items = inventory.get_items()
	var citem = inventory.get_item_in_position()
	var err_remove = inventory.remove_item(item1)
	var err_add_item3 = inventory.add_item(item3)
	var err_add_item1 = inventory.add_item(item1)
	var err_add_item2 = inventory.add_item(item2)
	var err_add_item22 = inventory.add_item(item2)
	inventory.sort(Inventory.BY_ALPHABET)
	inventory.sort(Inventory.BY_ID)
	var itemss = inventory.get_items()
	var item = inventory.get_item_in_position(1)
