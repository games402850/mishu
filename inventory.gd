extends Node
class_name Inventory

enum {BY_ALPHABET, BY_ID}

var _inventory_size : int
var _items : Array[Item]


func _init(inventory_size : int = 20):
	name = "Inventory"
	_inventory_size = inventory_size

func get_items() -> Array[Item]:
	print("%s: _items: %s" %[self, _items])
	return _items

func get_item_in_position(item_position : int = _items.size()) -> Item:
	if _items.is_empty() == true:
		printerr("%s: Out of range! Inventory is empty!" %self)
		return null
	
	print("%s: Item in position '%s': %s" %[self, item_position, _items[item_position]])
	return _items[item_position]

func add_item(item : Item) -> Error:
	if _items.size() >= _inventory_size:
		printerr("%s: Out of range! Inventory limit size: %s" %[self, _inventory_size])
		return ERR_PARAMETER_RANGE_ERROR
	_items.append(item)
	print("%s: Added item '%s'" %[self, _items[_items.size() - 1]])
	return OK

func remove_item(item : Item) -> Error:
	if _items.is_empty() == true:
		printerr("%s: Out of range! Inventory is empty!" %self)
		return ERR_PARAMETER_RANGE_ERROR
	
	if _items.has(item) == false:
		push_warning("%s: '%s' don't exist in inventory. Continue to work." %[self, item])
		return ERR_DOES_NOT_EXIST
	
	_items.remove_at(_items.bsearch(item))
	print("%s: Removed item '%s'" %[self, item])
	return OK

func sort(sort_method : int = Inventory.BY_ALPHABET) -> void:
	match sort_method:
		Inventory.BY_ALPHABET:
			_items.sort_custom(_sort_by_alphabet)
		Inventory.BY_ID:
			_items.sort_custom(_sort_by_id)
	print("%s: _items sorted '%s': %s" %[self, sort_method, _items])

func _sort_by_alphabet(a : Item, b : Item):
	if a._item_name > b._item_name:
		return true
	return false

func _sort_by_id(a : Item, b : Item):
	if a._id > b._id:
		return true
	return false
