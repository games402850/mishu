extends Resource
class_name Item

var _id : int
var _item_name : String
var _image_path : String

func _init(item_name : String = "Item", id : int = randi() % 100, image_path : String = "res://icon.svg"):
	resource_name = "Item"
	_id = id
	_item_name = item_name
	_image_path = image_path
